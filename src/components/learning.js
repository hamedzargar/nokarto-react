import React, { useState } from 'react';
const Learn = () => {
    const [MyWord , setMyWord] = useState('React');
    const [SomeTest , setSomeTest] = useState('');
    const spanClicked = ()=>{
        setMyWord(MyWord==='React' ? 'ES6':'React');
    }
    const Submit = ()=>{
        alert(SomeTest);
    }
    return (
        <div>
            <span onClick={spanClicked}>Learning {MyWord}</span>
            <input type="text" value={SomeTest} onChange={(e)=>{setSomeTest(e.target.value)}}/>
            <button onClick={Submit}>Click Me</button>
        </div>
    );
}

export default Learn;