import React, { useState } from 'react';
const Test = () => {
    const [ToggleStatus , setToggleStatus] = useState(false);
    const ToggleClick = () =>{
        setToggleStatus(ToggleStatus? false:true);
    }
    return (
        <div>
            <label style={{color:"red"}}>نام کاربری</label>
            <input type="username"/>
            <br />
            <label style={{color:"red"}}>کلمه عبور</label>
            <input type={ToggleStatus? "text" : "password"}/>
            <button onClick={ToggleClick}>Toggle</button>
            <br />
            <label style={{color:"orange"}}>نام پدر</label>
            <input type="father"/>
            <br />
            <label style={{color:"blue"}}>شغل</label>
            <input type="job"/>

        </div>
    );
}
 
export default Test;